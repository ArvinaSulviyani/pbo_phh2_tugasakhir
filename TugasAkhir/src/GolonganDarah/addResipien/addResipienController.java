package GolonganDarah.addResipien;

import GolonganDarah.database.DatabaseHandler;
import GolonganDarah.database.DbResipienModel;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import javafx.stage.Stage;

public class addResipienController implements Initializable {

    DatabaseHandler handler;
    DbResipienModel modelDb;

    @FXML
    private AnchorPane stagePane;

    @FXML
    private JFXTextField nama;

    @FXML
    private JFXTextField id;

    @FXML
    private JFXTextField gdarah;

    @FXML
    private JFXTextField telp;

    @FXML
    private JFXTextField alamat;

    @FXML
    private JFXButton save;

    @FXML
    private JFXButton cancel;

    @FXML
    void Cancel(ActionEvent event) {
        Stage stage =(Stage) stagePane.getScene().getWindow();
        stage.close();
    }

    @FXML
    void addResipien(ActionEvent event) {
        String namaResipien = nama.getText();
        String idResipien= id.getText();
        String gdarahResipien = gdarah.getText();
        String telpResipien = telp.getText();
        String alamatResipien = alamat.getText();


        Boolean flag = namaResipien.isEmpty() || idResipien.isEmpty() || gdarahResipien.isEmpty()   || telpResipien.isEmpty() || alamatResipien.isEmpty();
        if(flag) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setContentText("Isi Semua Data");
            alert.showAndWait();
            return;
        }

        addData();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        handler = DatabaseHandler.getInstance();
    }

    private void insert(DbResipienModel m){
        Connection connect = DatabaseHandler.tryConnect();
        PreparedStatement pStatement;
        try {
            String sql = "INSERT INTO resipien (nama, id, gdarah, telp, alamat) VALUES (?,?,?,?,?)";
            pStatement  = connect.prepareStatement(sql);
            pStatement.setString(1,m.getNama());
            pStatement.setString(2,m.getId());
            pStatement.setString(3,m.getGdarah());
            pStatement.setString(4,m.getTelp());
            pStatement.setString(5,m.getAlamat());
            pStatement.execute();


            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("Berhasil");
            alert.showAndWait();


        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private void addData(){
        modelDb = new DbResipienModel(nama.getText(),id.getText(), gdarah.getText(),  telp.getText(), alamat.getText());
        insert(modelDb);
        clearData();
    }

    private void clearData(){
        nama.clear();
        id.clear();
        gdarah.clear();
        telp.clear();
        alamat.clear();
    }

}
