package GolonganDarah.listPendonor;

import GolonganDarah.database.DatabaseHandler;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ListPendonorController implements Initializable {

    @FXML
    private AnchorPane stagePane;
    @FXML
    private TableView<Pendonor> tabelView;
    @FXML
    private TableColumn<Pendonor, String> namaCol;
    @FXML
    private TableColumn<Pendonor, String> idCol;
    @FXML
    private TableColumn<Pendonor, String> gdarahCol;
    @FXML
    private TableColumn<Pendonor, String> alamatCol;
    @FXML
    private TableColumn<Pendonor, String> rpenyakitCol;
    @FXML
    private TableColumn<Pendonor, String> statusCol;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        initCol();
        loadData();
    }

    private void initCol(){
        namaCol.setCellValueFactory(new PropertyValueFactory("nama"));
        idCol.setCellValueFactory(new PropertyValueFactory("id"));
        gdarahCol.setCellValueFactory(new PropertyValueFactory("gdarah"));
        alamatCol.setCellValueFactory(new PropertyValueFactory("alamat"));
        rpenyakitCol.setCellValueFactory(new PropertyValueFactory("rpenyakit"));
        statusCol.setCellValueFactory(new PropertyValueFactory("status"));
    }

    private void loadData(){
        DatabaseHandler handler = DatabaseHandler.getInstance();
        ObservableList<Pendonor> list = FXCollections.observableArrayList();
        try {
            Connection connect = DatabaseHandler.tryConnect();
            String sql = "select * from pendonor;";
            ResultSet rs   = connect.createStatement().executeQuery(sql);
            while(rs.next()){
                String nama = rs.getString("nama");
                String id = rs.getString("id");
                String gdarah = rs.getString("gdarah");
                String alamat = rs.getString("alamat");
                String rpenyakit = rs.getString("rpenyakit");
                Boolean status = rs.getBoolean("status");

                list.add(new Pendonor (nama, id, gdarah, alamat, rpenyakit, status));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListPendonorLoader.class.getName()).log(Level.SEVERE, null, ex);
        }

        tabelView.getItems().addAll(list);

    }

    public static class Pendonor{
        private final SimpleStringProperty nama;
        private final SimpleStringProperty id;
        private final SimpleStringProperty gdarah;
        private final SimpleStringProperty alamat;
        private final SimpleStringProperty rpenyakit;
        private final SimpleBooleanProperty status;

        Pendonor(String nama, String id, String gdarah, String alamat, String rpenya, Boolean status){
            this.nama = new SimpleStringProperty(nama);
            this.id = new SimpleStringProperty(id);
            this.gdarah = new SimpleStringProperty(gdarah);
            this.alamat = new SimpleStringProperty(alamat);
            this.rpenyakit = new SimpleStringProperty(rpenya);
            this.status = new SimpleBooleanProperty(status);
        }

        public String getNama() {
            return nama.get();
        }

        public String getId() {
            return id.get();
        }

        public String getGdarah() {
            return gdarah.get();
        }

        public String getAlamat() {
            return alamat.get();
        }

        public String getRpenyakit() {
            return rpenyakit.get();
        }

        public Boolean getStatus() {
            return status.get();
        }

    }

}
