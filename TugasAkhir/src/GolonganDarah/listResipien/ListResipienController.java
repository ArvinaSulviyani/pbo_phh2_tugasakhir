package GolonganDarah.listResipien;

import GolonganDarah.database.DatabaseHandler;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ListResipienController implements Initializable {

    @FXML
    private AnchorPane stagePane;
    @FXML
    private TableView<Resipien> tabelView;
    @FXML
    private TableColumn<Resipien, String> namaCol;
    @FXML
    private TableColumn<Resipien, String> idCol;
    @FXML
    private TableColumn<Resipien, String> gdarahCol;
    @FXML
    private TableColumn<Resipien, String> telpCol;
    @FXML
    private TableColumn<Resipien, String> alamatCol;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        initCol();
        loadData();
    }

    private void initCol(){
        namaCol.setCellValueFactory(new PropertyValueFactory("nama"));
        idCol.setCellValueFactory(new PropertyValueFactory("id"));
        gdarahCol.setCellValueFactory(new PropertyValueFactory("gdarah"));
        telpCol.setCellValueFactory(new PropertyValueFactory("telp"));
        alamatCol.setCellValueFactory(new PropertyValueFactory("alamat"));
    }

    private void loadData(){
        DatabaseHandler handler = DatabaseHandler.getInstance();
        ObservableList<Resipien> list = FXCollections.observableArrayList();
        try {
            Connection connect = DatabaseHandler.tryConnect();
            String sql = "select * from resipien;";
            ResultSet rs   = connect.createStatement().executeQuery(sql);
            while(rs.next()){
                String nama = rs.getString("nama");
                String id = rs.getString("id");
                String gdarah = rs.getString("gdarah");
                String telp = rs.getString("telp");
                String alamat = rs.getString("alamat");

                list.add(new Resipien (nama, id, gdarah, telp, alamat));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ListResipien.class.getName()).log(Level.SEVERE, null, ex);
        }

        tabelView.getItems().addAll(list);

    }

    public static class Resipien{
        private final SimpleStringProperty nama;
        private final SimpleStringProperty id;
        private final SimpleStringProperty gdarah;
        private final SimpleStringProperty telp;
        private final SimpleStringProperty alamat;

        Resipien(String nama, String id, String gdarah, String telp, String alamat){
            this.nama = new SimpleStringProperty(nama);
            this.id = new SimpleStringProperty(id);
            this.gdarah = new SimpleStringProperty(gdarah);
            this.telp = new SimpleStringProperty(telp);
            this.alamat = new SimpleStringProperty(alamat);
        }

        public String getNama() {
            return nama.get();
        }

        public String getId() {
            return id.get();
        }

        public String getGdarah() {
            return gdarah.get();
        }

        public String getTelp() {
            return telp.get();
        }

        public String getAlamat() {
            return alamat.get();
        }
    }

}
