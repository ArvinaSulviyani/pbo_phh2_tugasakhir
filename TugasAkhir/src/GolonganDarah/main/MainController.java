package GolonganDarah.main;

import GolonganDarah.database.DatabaseHandler;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.effects.JFXDepthManager;;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;

import java.io.IOException;
import java.net.URL;
import java.sql.*;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.event.ActionEvent;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


public class MainController implements Initializable {

    DatabaseHandler databaseHandler;

    @FXML
    private HBox infoPendonor;

    @FXML
    private HBox infoResipien;

    @FXML
    private Text namaPendonor;

    @FXML
    private Text gdarahPendonor;

    @FXML
    private Text statusPendonor;

    @FXML
    private TextField inputIdResipien;

    @FXML
    private Text namaResipien;

    @FXML
    private Text telpResipien;

    @FXML
    private JFXTextField IDPendonor;

    @FXML
    private TextField inputIdPendonor;

    @FXML
    private ListView<String> dataList;

    @FXML
    void loadAddPedonor(ActionEvent event) {
        loadWindow("/GolonganDarah/addPendonor/addPendonor.fxml", "Add New Pendonor");
    }

    @FXML
    void loadAddResipien(ActionEvent event) {
        loadWindow("/GolonganDarah/addResipien/addResipien.fxml", "Add New Resipien");
    }

    @FXML
    void loadListPendonor(ActionEvent event) {
        loadWindow("/GolonganDarah/listPendonor/ListPendonor.fxml", "List Pendonor");
    }

    @FXML
    void loadListResipien(ActionEvent event) {
        loadWindow("/GolonganDarah/listResipien/ListResipien.fxml", "List Resipien");
    }

    @FXML
    void about(ActionEvent event) {
        loadWindow("/GolonganDarah/about/About.fxml", "About");
    }

    @FXML
    private void loadInfoPendonor(ActionEvent event) {
        clearPendonor();

        String id =  inputIdPendonor.getText();
        String sql = "select * from pendonor where id = '" + id + "'";
        ResultSet rs = DatabaseHandler.exeQuery(sql);
        Boolean flag = false;
        try {
            while(rs.next()){
                String namaP = rs.getString("nama");
                String gdarahP = rs.getString("gdarah");
                Boolean statusP = rs.getBoolean("status");
                namaPendonor.setText(namaP);
                gdarahPendonor.setText(gdarahP);
                String status = (statusP)?"Ada" : "Tidak Ada";
                statusPendonor.setText(status);
                flag = true;
            }
            if(!flag){
                namaPendonor.setText("Info ID Pendonor Tidak Ditemukan");
            }
        } catch (SQLException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void loadInfoResipien(ActionEvent event) {
        clearResipien();

        String id =  inputIdResipien.getText();
        String sql = "select * from resipien where id = '" + id + "'";
        ResultSet rs = DatabaseHandler.exeQuery(sql);
        Boolean flag = false;
        try {
            while(rs.next()){
                String namaP = rs.getString("nama");
                String telpP = rs.getString("telp");

                namaResipien.setText(namaP);
                telpResipien.setText(telpP);

                flag = true;
            }
            if(!flag){
                namaResipien.setText("Info ID Resipien Tidak Ditemukan");
            }
        } catch (SQLException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void loadIssue (ActionEvent event) {
        String idResipien = inputIdResipien.getText();
        String idPendonor = inputIdPendonor.getText();

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Konfimasi");
        alert.setHeaderText(null);
        alert.setContentText("Apakah anada yakin ingin meminta darah dari " + namaPendonor.getText() + " ?");

        Optional<ButtonType> respon = alert.showAndWait();
        if(respon.get() == ButtonType.OK){
            Connection connect = DatabaseHandler.tryConnect();
            PreparedStatement pStatement;
            try {
                String sql = "INSERT INTO data (IdPendonor, IdResipien) VALUES (?,?)";
                pStatement  = connect.prepareStatement(sql);
                pStatement.setString(1,idPendonor);
                pStatement.setString(2,idResipien);
                pStatement.execute();

                String sql2 = "update pendonor set status = 0 where id = '"+ idPendonor + "'";
                pStatement  = connect.prepareStatement(sql2);
                pStatement.execute();

                Alert alert1 = new Alert(Alert.AlertType.INFORMATION);
                alert1.setHeaderText(null);
                alert1.setContentText("Berhasil");
                alert1.showAndWait();


            } catch (SQLException e) {
                Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, e);
            }

        }
        else {
            Alert alert1 = new Alert(Alert.AlertType.INFORMATION);
            alert1.setHeaderText(null);
            alert1.setContentText("Keluar");
            alert1.showAndWait();
        }
    }

    @FXML
    void loadInfoPendonor2(ActionEvent event) {
        ObservableList<String> data = FXCollections.observableArrayList();

        String Id = IDPendonor.getText();
        String sql = "select * from data where IdPendonor = '" + Id + "'";
        ResultSet rs = databaseHandler.exeQuery(sql);
        try {
            while(rs.next()){
                String mIdPendonor  = Id;
                String mIdResipien = rs.getString("IdResipien");
                Timestamp mTime = rs.getTimestamp("tanggal");

                data.add("Tanggal dan Waktu : " + mTime.toGMTString());

                data.add("Informasi Pendonor : ");
                sql = "select * from pendonor where id = '" + mIdPendonor + "'";
                ResultSet rs1 = databaseHandler.exeQuery(sql);
                while(rs1.next()){
                    data.add("      Nama Pendonor : " + rs1.getString("nama"));
                    data.add("      ID Pendonor : " + rs1.getString("id"));
                    data.add("      Golongan Darah Pendonor : " + rs1.getString("gdarah"));
                    data.add("      Alamat Pendonor : " + rs1.getString("alamat"));
                    data.add("      Riwayat Penyakit Pendonor : " + rs1.getString("rpenyakit"));
                }

                data.add("Informasi Resipien : ");
                sql = "select * from resipien where id = '" + mIdResipien + "'";
                rs1 = databaseHandler.exeQuery(sql);
                while(rs1.next()){
                    data.add("      Nama Resipien : " + rs1.getString("nama"));
                    data.add("      ID Resipien : " + rs1.getString("id"));
                    data.add("      No.Telp Resipien : " + rs1.getString("telp"));
                    data.add("      Alamat Resipien : " + rs1.getString("alamat"));
                }
            }
        }
        catch (SQLException ex){
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }

        dataList.getItems().setAll(data);

    }



    @Override
    public void initialize(URL url, ResourceBundle rb) {
        JFXDepthManager.setDepth(infoPendonor, 1);
        JFXDepthManager.setDepth(infoResipien, 1);

        databaseHandler = DatabaseHandler.getInstance();

    }

    void clearPendonor(){
        namaPendonor.setText("");
        gdarahPendonor.setText("");
        statusPendonor.setText("");
    }

    void clearResipien(){
        namaResipien.setText("");
        telpResipien.setText("");
    }

    void loadWindow(String loc, String title){
        try{
            Parent parent = FXMLLoader.load(getClass().getResource(loc));
            Stage stage = new Stage(StageStyle.DECORATED);
            stage.setTitle(title);
            stage.setScene(new Scene(parent));
            stage.show();
        }
        catch (IOException e){
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, e);
        }


    }



}
