package GolonganDarah.addPendonor;

import GolonganDarah.database.DatabaseHandler;
import GolonganDarah.database.DbPendonorModel;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class addPendonorController implements Initializable {

    @FXML
    private AnchorPane stagePane;
    @FXML
    private JFXTextField nama;
    @FXML
    private JFXTextField id;
    @FXML
    private JFXTextField gdarah;
    @FXML
    private JFXTextField alamat;
    @FXML
    private JFXTextField rpenyakit;
    @FXML
    private JFXButton save;
    @FXML
    private JFXButton cancel;


    DatabaseHandler databaseHandler;
    DbPendonorModel modelDb;


    @FXML
    void Cancel(ActionEvent event) {
        Stage stage =(Stage) stagePane.getScene().getWindow();
        stage.close();
    }

    @FXML
    void addPendonor(ActionEvent event) {
        String namaPendonor = nama.getText();
        String idPendonor = id.getText();
        String gdarahPendonor = gdarah.getText();
        String alamatPendonor = alamat.getText();
        String rpenyakitPendonor = rpenyakit.getText();

        if(namaPendonor.isEmpty() || idPendonor.isEmpty() || gdarahPendonor.isEmpty()  || alamatPendonor.isEmpty() || rpenyakitPendonor.isEmpty()){
            Alert alert = new Alert (Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setContentText("Isi Semua Data");
            alert.showAndWait();
            return;
        }

        addData();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        databaseHandler = DatabaseHandler.getInstance();
    }

    private void insert(DbPendonorModel m){
        Connection connect = DatabaseHandler.tryConnect();
        PreparedStatement pStatement;
        try {
            String sql = "INSERT INTO pendonor (nama, id, gdarah, alamat, rpenyakit) VALUES (?,?,?,?,?)";
            pStatement  = connect.prepareStatement(sql);
            pStatement.setString(1,m.getNama());
            pStatement.setString(2,m.getId());
            pStatement.setString(3,m.getGdarah());
            pStatement.setString(4,m.getAlamat());
            pStatement.setString(5,m.getRpenyakit());

            pStatement.execute();

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setContentText("Berhasil");
            alert.showAndWait();


        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private void addData(){
        modelDb = new DbPendonorModel(nama.getText(),id.getText(), gdarah.getText(),alamat.getText(), rpenyakit.getText());
        insert(modelDb);
        clearData();
    }

    private void clearData(){
        nama.clear();
        id.clear();
        gdarah.clear();
        alamat.clear();
        rpenyakit.clear();
    }

}
