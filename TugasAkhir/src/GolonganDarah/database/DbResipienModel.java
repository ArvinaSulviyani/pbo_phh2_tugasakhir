package GolonganDarah.database;

import GolonganDarah.listPendonor.ListPendonorController;
import javafx.beans.property.SimpleStringProperty;


public class DbResipienModel {
    public final SimpleStringProperty nama;
    public final SimpleStringProperty id;
    public final SimpleStringProperty gdarah;
    public final SimpleStringProperty telp;
    public final SimpleStringProperty alamat;



    public DbResipienModel(String fnama, String fid, String fgdarah, String ftelp, String falamat){
        this.nama = new SimpleStringProperty(fnama);
        this.id = new SimpleStringProperty(fid);
        this.gdarah = new SimpleStringProperty(fgdarah);
        this.telp = new SimpleStringProperty(ftelp);
        this.alamat = new SimpleStringProperty(falamat);
    }

    public String getNama() {
        return nama.get();
    }

    public void setNama(String value) {
        nama.set(value);
    }

    public String getId() {
        return id.get();
    }

    public void setId(String value) {
        id.set(value);
    }

    public String getGdarah() {
        return gdarah.get();
    }

    public void setGdarah(String value) {
        gdarah.set(value);
    }

    public String getTelp() {
        return telp.get();
    }

    public void setTelp(String value) {
        telp.set(value);
    }

    public String getAlamat() {
        return alamat.get();
    }

    public void setAlamat(String value) {
        alamat.set(value);
    }
}