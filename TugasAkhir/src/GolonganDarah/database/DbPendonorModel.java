package GolonganDarah.database;

import javafx.beans.property.SimpleStringProperty;


public class DbPendonorModel {
    public final SimpleStringProperty nama;
    public final SimpleStringProperty id;
    public final SimpleStringProperty gdarah;
    public final SimpleStringProperty alamat;
    public final SimpleStringProperty rpenyakit;



    public DbPendonorModel(String fnama, String fid, String fgdarah, String falamat, String frpenyakit){
        this.nama = new SimpleStringProperty(fnama);
        this.id = new SimpleStringProperty(fid);
        this.gdarah = new SimpleStringProperty(fgdarah);
        this.alamat = new SimpleStringProperty(falamat);
        this.rpenyakit = new SimpleStringProperty(frpenyakit);

    }

    public String getNama() {
        return nama.get();
    }

    public void setNama(String value) {
        nama.set(value);
    }

    public String getId() {
        return id.get();
    }

    public void setId(String value) {
        id.set(value);
    }

    public String getGdarah() {
        return gdarah.get();
    }

    public void setGdarah(String value) {
        gdarah.set(value);
    }

    public String getAlamat() {
        return alamat.get();
    }

    public void setAlamat(String value) {
        alamat.set(value);
    }

    public String getRpenyakit() {
        return rpenyakit.get();
    }

    public void setRpenyakit(String value) {
        rpenyakit.set(value);
    }

  /*  public String getStatus() {
        return status.get();
    }*/
}