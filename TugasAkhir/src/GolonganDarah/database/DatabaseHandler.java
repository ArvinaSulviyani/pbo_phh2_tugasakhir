package GolonganDarah.database;

import java.sql.*;

public class DatabaseHandler {
    private static Connection connect;
    private static Statement statement;
    private static DatabaseHandler handler = null;


    public static Connection tryConnect(){
        if (connect == null) {
            try {
                String url = "jdbc:mysql://localhost:3306/golongan_darah";
                String user = "root";
                String pass = "";
                DriverManager.registerDriver(new com.mysql.jdbc.Driver());
                connect = DriverManager.getConnection(url, user, pass);
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return connect;
    }

    public static ResultSet exeQuery(String sql){
        ResultSet result;
        try{
            statement = tryConnect().createStatement();
            result = statement.executeQuery(sql);
        }
        catch (SQLException e){
            System.out.println(e.getLocalizedMessage());
            return null;
        } finally {
        }
        return result;
    }

    public static Boolean exeAction(String sql){
        try{
            statement = tryConnect().createStatement();
            statement.execute(sql);
            return true;
        }
        catch (SQLException e){
            System.out.println(e.getLocalizedMessage());
            return false;
        } finally {
        }
    }

    public static DatabaseHandler getInstance(){
        if(handler == null){
            handler = new DatabaseHandler();
        }
        return handler;
    }

}

