-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 19 Des 2017 pada 09.40
-- Versi Server: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `golongan_darah`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `data`
--

CREATE TABLE `data` (
  `IdPendonor` varchar(40) NOT NULL,
  `IdResipien` varchar(40) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data`
--

INSERT INTO `data` (`IdPendonor`, `IdResipien`, `tanggal`) VALUES
('GL001', 'R100', '2017-12-16 14:44:22'),
('GL002', 'R100', '2017-12-16 14:48:53'),
('GL004', 'R100', '2017-12-16 16:38:11'),
('GL003', 'R100', '2017-12-17 04:34:09'),
('GL005', 'R100', '2017-12-17 05:08:08'),
('GL100', 'R100', '2017-12-19 06:37:13');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendonor`
--

CREATE TABLE `pendonor` (
  `nama` varchar(40) NOT NULL,
  `id` varchar(40) NOT NULL,
  `gdarah` varchar(40) NOT NULL,
  `alamat` varchar(40) NOT NULL,
  `rpenyakit` varchar(40) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pendonor`
--

INSERT INTO `pendonor` (`nama`, `id`, `gdarah`, `alamat`, `rpenyakit`, `status`) VALUES
('Arvina', 'GL001', 'O', 'Bone', 'Tidak Ada', 0),
('Ana', 'GL002', 'AB', 'Makassar', 'Tipes', 0),
('Hajrah', 'GL003', 'A', 'Bone', 'Asma', 0),
('Fifi', 'GL004', 'A', 'Bone', 'Maag', 0),
('Wawan', 'GL005', 'O', 'Bone', 'Tidak Ada', 0),
('Sherly', 'GL006', 'O', 'Bone', 'Tidak Ada', 1),
('Kiki', 'GL007', 'AB', 'Makassar', 'Asma', 1),
('Nurfadillah', 'GL008', 'O', 'Enrekang', 'Tidak Ada', 1),
('Dewi', 'GL100', 'B', 'Bone', 'Tipes', 0),
('Tini', 'GL101', 'O', 'Toraja', 'Tidak ada', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `resipien`
--

CREATE TABLE `resipien` (
  `nama` varchar(40) NOT NULL,
  `id` varchar(40) NOT NULL,
  `gdarah` varchar(40) NOT NULL,
  `telp` varchar(40) NOT NULL,
  `alamat` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `resipien`
--

INSERT INTO `resipien` (`nama`, `id`, `gdarah`, `telp`, `alamat`) VALUES
('Min Kyu', 'R100', 'O', '0987654321', 'Busan');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pendonor`
--
ALTER TABLE `pendonor`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `resipien`
--
ALTER TABLE `resipien`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
